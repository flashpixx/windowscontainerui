# Wine

Regularly updated linux-based Ubuntu Rolling [Wine](https://www.winehq.org/) container image for running Windows application (Putty as example) with X11 and [IceWM](https://ice-wm.org/). The UI can be accessed via the browser.

![](screen.png)

### Workflow

1. run the image with

   ```
   docker run --rm -it -p 8080:8080 flashpixx/wine
   ```

2. open the browser on [http://localhost:8080/](http://localhost:8080/) and click login (no password is used)
3. on IceWM the menu can be opend with a _right click_ on the desktop and the application will be automatically restarted after closing
