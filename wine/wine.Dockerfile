# === wine base image for windows 64bit executables
FROM ubuntu:rolling AS base

# --- environment setup with user home, display for vnc and nginx port
ARG USER=ubuntu

ENV DEBIAN_FRONTEND=noninteractive \
    HOME=/home/${USER} \
    USER=${USER} \
    DISPLAY=":1"

WORKDIR /home/${USER}
EXPOSE 8080


# --- for windows 32bit executables add "dpkg --add-architecture i386" and package "wine32"
RUN apt-get update &&\
    apt-get install -y \
        tightvncserver icewm \
        websockify novnc supervisor nginx \
        wine64 libwine \
        wget curl make &&\
    apt-get clean autoclean &&\
    apt-get -y autoremove --purge &&\
    rm -rf /var/lib/apt/lists/*


# --- setup data with user home directory and vnc and wine config
COPY ./base /
RUN chown -R ${USER}:${USER} /var/lib/nginx &&\
    chown -hR ${USER}:${USER} $HOME &&\
    su ${USER} -c 'mkdir -p $HOME/.vnc' &&\
    su ${USER} -c 'echo "nopass" | vncpasswd -f > $HOME/.vnc/passwd' &&\
    su ${USER} -c 'wineboot --update'


# --- start supervisore daemon to start nginx, vnc, icewm and windows application
CMD ["/usr/bin/supervisord", "-s", "-j", "/var/run/supervisord.pid", "-l", "/dev/null", "-c", "/etc/supervisor/conf.d/supervisord.conf"]



# === windows application image
FROM base AS putty

RUN wget -qO /opt/putty.exe https://the.earth.li/~sgtatham/putty/latest/w64/putty.exe
COPY ./putty /
